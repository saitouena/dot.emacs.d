;;; -*- no-byte-compile: t -*-
(define-package "git-commit" "20190319.2352" "Edit Git commit messages" '((emacs "25.1") (dash "20180910") (with-editor "20181103")) :commit "0abc761f556c746269932b0c5a9c020e63965471" :keywords '("git" "tools" "vc") :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")
