(require 'package)

;; MELPAを追加
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

;; MELPA-stableを追加
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;; Marmaladeを追加
;;(add-to-list 'package-archives  '("marmalade" . "http://marmalade-repo.org/packages/") t)

;; Orgを追加
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

;; 初期化
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (cider mozc ag nginx-mode ac-slime yaml-mode haskell-mode company pg markdown-mode vue-mode open-junk-file clojure-mode proof-general magit paredit php-mode)))
 '(safe-local-variable-values
   (quote
    ((cider-refresh-after-fn . "zou.framework.repl/go")
     (cider-refresh-before-fn . "zou.framework.repl/stop")
     (cider-cljs-lein-repl . "(zou.framework.repl/cljs-repl)")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'paredit)
(show-paren-mode 1)
(add-hook 'clojure-mode-hook 'enable-paredit-mode)
(add-hook 'scheme-mode-hook 'enable-paredit-mode)
(add-hook 'lisp-mode-hook 'enable-paredit-mode)
(add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)

;; from https://qiita.com/sune2/items/b73037f9e85962f5afb7
(require 'company)
(global-company-mode) ; 全バッファで有効にする 
(setq company-idle-delay 0) ; デフォルトは0.5
(setq company-minimum-prefix-length 2) ; デフォルトは4
(setq company-selection-wrap-around t) ; 候補の一番下でさらに下に行こうとすると一番上に戻る

(setq column-number-mode t)

;; https://asukiaaa.blogspot.com/2017/12/emacsslimeroswell.html
(load (expand-file-name "~/.roswell/helper.el"))

(load (expand-file-name "~/.emacs.d/copl-mode/copl-mode.el"))

;; japanese input
;; https://www.crz33.com/software/env/emacs
;; ***your system requires /usr/bin/mozc_emacs_helper***
(set-language-environment "Japanese")
(setq default-input-method "japanese-mozc")

